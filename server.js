const express = require('express')
const app = express()

let todos = [
    {
        name : 'Joe',
        id : 1
    },
    {
        name : 'Nontawat',
        id : 2
    }
]
//SELECT
app.get('/todos', (req,res) => {
    res.send(todos)
})
app.post('/todos',(req,res) => {
    let newTodo = {
        name : 'Read a book',
        id : 3
    }
    todos.push(newTodo)
    res.status(201).send()
})

app.listen(3000,() => {
    console.log('Todo api started at port 3000')
})